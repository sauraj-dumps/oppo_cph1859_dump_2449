## aosp_CPH1859-eng 12 SP1A.210812.016 eng.kardeb.20211008.032020 test-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1859
- Brand: oppo
- Flavor: aosp_CPH1859-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.kardeb.20211008.032020
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/aosp_CPH1859/CPH1859:12/SP1A.210812.016/kardebayan310080427:eng/test-keys
- OTA version: 
- Branch: aosp_CPH1859-eng-12-SP1A.210812.016-eng.kardeb.20211008.032020-test-keys
- Repo: oppo_cph1859_dump_2449


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
